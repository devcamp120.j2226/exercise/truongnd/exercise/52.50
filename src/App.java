import java.util.ArrayList;
import java.util.Date;
import com.devcamp.j03_javabasic.s50.Order;
public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> arrayList = new ArrayList<>();
        //Khởi tạo person với các tham số khác nhau
        Order order0 = new Order();
        Order order1 = new Order("Lan");
        Order order2 = new Order(3,"Long",80000);
        Order order3 = new Order(4,"Nam",75000,new Date(),false,new String[]{"Hop Mau","Tay","Giay Mau"});
        //thêm object person vào danh sách
        arrayList.add(order0);
        arrayList.add(order1);
        arrayList.add(order2);
        arrayList.add(order3);
        //in ra màn hình
        for (Order order : arrayList){
            System.out.println(order.toString());
        }
    }
}
